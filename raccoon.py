# !/usr/bin/env python
# -*- coding: utf-8 -*-
# @Date     : 2018/11/26
# @Author   : Deyong ZHAN
# @Version  : v1.0
# 个人数据清洗包raccoon首次正式发布

import datetime as dt
import numpy as np
import os
import pandas as pd
import random
import re
import time
import traceback
import warnings
warnings.filterwarnings("ignore")


def make_directory(path):
    """
    在本地生成路径
    """
    if not os.path.exists(path):
        os.mkdir(path)


def blank_check(df, columns):
    """
    检查是否存在空格
    :param df: DataFrame, 待检查的数据表名称
    :param columns: list, 需要执行正空格检查的列名
    :return: None
    """
    def upper_letter_filter(x):
        x = str(x)
        if re.search('\s', x):  # [<空格>\t\r\n\f\v]
            return True
        else:
            return False
    df2 = df.copy()
    check_bool = df2[columns].applymap(upper_letter_filter)
    df3 = df2[columns][check_bool.sum(1) > 0]
    if len(df3):
        print("\n下列数据取值存在空白字符")
        print(df3)
    else:
        print("\n检查完毕，未发现空格")


def drop_blank(df, columns=None):
    """
    剔除空格
    :param df: DataFrame
    :param columns: list, 待删除空格的列名
    :return: DataFrame
    """
    if not columns:
        columns = df.columns
    df2 = df.copy()
    df2[columns] = df2[columns].applymap(lambda x: str(x).replace(" ", ""))
    return df2


def mchnt_match_where_condition(df, white={"mchnt_name": "商户关键字白名单"}, black={"mchnt_name": "商户关键字黑名单"}):
    """
    基于黑白名单生成HQL中的筛选条件(where语句)
    :param df: DataFrame, 含有关键字白/黑名单（正则表达式）的品牌对应关键字表
    :param white: dict, 数据库字段名：关键字表相应的白名单字段名
    :param black: dict, 数据库字段名：关键字表相应的黑名单字段名
    :return: Series
    """
    def where_sql_func(x, w, b):
        condition = list()
        for col in w.keys():
            if x[w[col]] in ['|', "nan", np.NaN, np.nan, np.NAN, None]:
                continue
            elif col == "mchnt_name":  # 商户名称需要统一转成小写进行匹配
                condition.append("lower(" + col + ") rlike \"" + x[w["mchnt_name"]] + "\"")
            else:
                condition.append(col + " rlike \"" + str(x[w[col]]) + "\"")

        for col in b.keys():
            if x[b[col]] in ['/', "nan", np.NaN, np.nan, np.NAN, None]:
                continue
            elif col == "mchnt_name":
                condition.append("lower(" + col + ") not rlike \"" + x[b["mchnt_name"]] + "\"")
            else:
                condition.append(col + " not rlike \"" + str(x[b[col]]) + "\"")
        return "(" + " and ".join(condition) + ")"
    columns = list(white.values()) + list(black.values())
    return df[columns].apply(where_sql_func, axis=1, w=white, b=black)


def where_sql_func(x, w, b):
    # 基于各字段的黑白规则生成相应的HQL筛选条件(where语句)
    condition = list()
    for col in w.keys():
        if x[w[col]] in ['|', "nan", np.NaN, np.nan, np.NAN, None]:
            continue
        elif col == "mchnt_name":  # 商户名称需要统一转成小写进行匹配
            condition.append("lower(" + col + ") rlike \"" + x[w["mchnt_name"]] + "\"")
        else:
            condition.append(col + " rlike \"" + str(x[w[col]]) + "\"")

    for col in b.keys():
        if x[b[col]] in ['/', "nan", np.NaN, np.nan, np.NAN, None]:
            continue
        elif col == "mchnt_name":
            condition.append("lower(" + col + ") not rlike \"" + x[b["mchnt_name"]] + "\"")
        else:
            condition.append(col + " not rlike \"" + str(x[b[col]]) + "\"")
    return "(" + " and ".join(condition) + ")"


def where_condition(df, white={"mchnt_name": "商户关键字白名单"}, black={"mchnt_name": "商户关键字黑名单"}):
    """
    基于各字段的黑白规则生成相应的HQL筛选条件(where语句)
    :param df: DataFrame, 含有各字段白/黑规则（正则表达式）的规则表
    :param white: dict, 数据库字段名：相应的白名单字段名
    :param black: dict, 数据库字段名：关键字表相应的黑名单字段名
    :return: Series
    """
    columns = list(white.values()) + list(black.values())
    return df[columns].apply(where_sql_func, axis=1, w=white, b=black)


def merchant_matching_hql(df, new_table, location, from_date='2015-01-01', to_date='2018-05-31',
                          acpt_resp_cd=None, trans_at=None, trans_id=None, trans_status=None, where_hql='where_hql',
                          head=True):
    """
    批量生成商户名称匹配HQL
    :param df: DataFrame, 含有商户白/黑名单关键字匹配where子句的品牌对应关键字表
    :param new_table: str, 新建表名称，用于存放查询结果
    :param location: str, hadoop存储位置
    :param from_date: str, 流水日期筛选起点，yyyy-mm-dd格式
    :param to_date: str, 流水日期筛选终点，yyyy-mm-dd格式
    :param acpt_resp_cd: str, 应答码字段筛选条件，如“acpt_resp_cd = 00"
    :param trans_at: str, 交易金额字段筛选条件，如”trans_at > 1"
    :param head: bool, hive脚本开头设置内容
    :return: str, hql
    """
    drop_tab = "\n\ndrop table if exists " + new_table + ";"
    create_tab = "\ncreate table " + new_table + " stored as orcfile location '" + location + "' as\nselect distinct"
    select_col = "\n\tmchnt_tp, mchnt_name, mchnt_cd, term_id, substr(acpt_ins_id_cd, -4, 4) as city_cd"
    from_tab = "\nfrom dw.tbl_fct_trans_sm3 "
    dt_condition = "\nwhere dt between '" + from_date + "' and '" + to_date + "'"
    if acpt_resp_cd:
        acptt_resp_cd_condition = "\n\tand " + acpt_resp_cd  # 应答码
    else:
        acptt_resp_cd_condition = ""
    if trans_at:  # 金额（分）
        trans_at_condition = "\n\tand " + trans_at
    else:
        trans_at_condition = ""
    if trans_id:  # 交易代码
        trans_id_condition = "\n\tand " + trans_id
    else:
        trans_id_condition = ""
    if trans_status:  # 交易状态
        trans_status_condition = "\n\tand " + trans_status
    else:
        trans_status_condition = ""
    mchnt_name_condition = "\n\tand (\n\t\t" + " or\n\t\t".join(df[where_hql]) + "\n\t\t)"
    where_condition = dt_condition + acptt_resp_cd_condition + trans_at_condition + trans_id_condition \
                      + trans_status_condition + mchnt_name_condition
    #group_by = "\ngroup by mchnt_tp, mchnt_name, mchnt_cd, term_id, substr(acpt_ins_id_cd, -4, 4)"
    if head:
        head = "\nset mapreduce.job.queuename=root.Upsmart.Upsmart_queue2;" \
               "\nset mapred.reduce.tasks = 60;" \
               "\nset hive.auto.convert.join = true;" \
               "\nuse default_00010018_zhandy;\n"
        hql = head + drop_tab + create_tab + select_col + from_tab + where_condition + ";"
    else:
        hql = "\n" + drop_tab + create_tab + select_col + from_tab + where_condition + ";"
    return hql


def type_matching_hql(df, new_table, location, from_table, type1='公司代码', type2='品牌代码', col1='type1', col2='type2',
                      where_hql='where_hql', head=True):
    """
    基于规则表生成type匹配HiveSQL
    :param df: DataFrame, 含有商户白/黑名单关键字匹配where子句的品牌对应关键字表
    :param new_table: str, 新建表名称，用于存放查询结果
    :param location: str, hadoop存储位置
    :param from_table: str, 所查询的表名称（原始商户名称表）
    :param type1: str, type1列名
    :param type2: str, type2列名
    :param col1: str, hive数据表相应的type1列名
    :param col2: str, hive数据表相应的type2列名
    :param where_hql: str, where筛选条件所在列名
    :param head: bool, hive脚本开头设置内容
    :return: str, hql
    """
    drop_tab = "\n\ndrop table if exists " + new_table + ";"
    create_select = "\ncreate table " + new_table + " stored as orcfile location '" + location + "' as\nselect distinct a.*"
    sub_table = "select \"" + df[type1] + "\" as " + col1 + ", \"" + df[type2] + "\" as " + col2 + ", * from " \
                + from_table + " where " + df[where_hql]
    from_tab = "\nfrom\n\t(\n\t" + " union all\n\t".join(sub_table) + "\n\t) a;"
    if head:
        head = "\nset mapreduce.job.queuename=root.Upsmart.Upsmart_queue2;" \
               "\nset mapred.reduce.tasks = 60;" \
               "\nset hive.auto.convert.join = true;" \
               "\nuse default_00010018_zhandy;\n"
        hql = head + drop_tab + create_select + from_tab
    else:
        hql = "\n" + drop_tab + create_select + from_tab
    return hql


def tag_matching_hql(df, new_table, location, from_table, limit=None, header=True, name_rule={}, equal_rule={}, white_rule={}, black_rule={}):
    """
    基于规则表生成标签匹配HiveSQL
    :param df: DataFrame, 匹配规则表
    :param new_table: str, 数据库新建表名称，用于存放查询结果
    :param location: str, 数据库新建表的存储路径
    :param from_table: str, 数据库用来匹配标签的原始表名称
    :param limit: int, HiveSQL脚本中的limit取值
    :param header: bool, 是否添加HiveSQL开头初始化设置部分的脚本
    :param name_rule: dict, 数据库新增的标签字段名：规则表相应的标签值字段名
    :param equal_rule: dict, 数据库执行相等规则字段名：规则表相应的相等规则字段名
    :param white_rule: dict, 数据库执行白名单规则字段名：规则表相应的白名单规则字段名
    :param black_rule: dict, 数据库执行黑名单规则字段名：规则表相应的黑名单规则字段名
    :return: str, HiveSQL脚本
    """
    if name_rule:
        as_hql = df[list(set(name_rule.values()))].apply(
            lambda x: " ".join(["\"" + str(x[name_rule[col]]) + "\" as " + str(col) + "," for col in name_rule.keys()]),
            axis=1)
    else:
        as_hql = ""
    if equal_rule:
        where_hql1 = df[list(set(equal_rule.values()))].apply(
            lambda x: "and ".join([str(col) + "= \"" + str(x[equal_rule[col]]) + "\"" for col in equal_rule.keys()]), axis=1)
    else:
        where_hql1 = ""

    columns = list(set(list(white_rule.values()) + list(black_rule.values())))
    if columns:
        where_hql2 = df[columns].apply(where_sql_func, axis=1, w=white_rule, b=black_rule)
    else:
        where_hql2 = ""

    if len(equal_rule)*len(columns) > 0:
        where_hql = where_hql1 + " and " + where_hql2
    else:
        where_hql = where_hql1 + where_hql2

    drop_tab = "\n\ndrop table if exists " + new_table + ";"
    create_select = "\ncreate table " + new_table + " stored as orcfile location '" + location + "' as\nselect distinct a.*"

    if limit:
        sub_table = "select " + as_hql + " * from " + from_table + " where " + where_hql + " limit " + str(int(limit))
    else:
        sub_table = "select " + as_hql + " * from " + from_table + " where " + where_hql

    from_tab = "\nfrom\n\t(\n\t" + " union all\n\t".join(sub_table) + "\n\t) a;"

    if header:
        head = "\nset mapreduce.job.queuename=root.Upsmart.Upsmart_queue2;" \
               "\nset mapred.reduce.tasks = 60;" \
               "\nset hive.auto.convert.join = true;" \
               "\nuse default_00010018_zhandy;\n"
        hql = head + drop_tab + create_select + from_tab
    else:
        hql = "\n" + drop_tab + create_select + from_tab
    return hql


def merchant_clean_where_condition(df, limit, type1='type1', type2='type2', col1='type1', col2='type2'):
    if df['name_white'] in ['|', "nan", np.NaN, np.nan, np.NAN]:
        name_white_condition = ""
    else:
        name_white_condition = " and lower(mchnt_name) rlike \"" + df['name_white'] + "\""
    if df['citycode_white'] in ['|', "nan", np.NaN, np.nan, np.NAN]:
        citycode_white_condition = ""
    else:
        citycode_white_condition = " and city_cd rlike \"" + df['citycode_white'] + "\""
    if df['name_black'] in ['/', "nan", np.NaN, np.nan, np.NAN]:
        name_black_condition = ""
    else:
        name_black_condition = " and lower(mchnt_name) not rlike \"" + df['name_black'] + "\""
    if df['mcc_white'] in ['|', "nan", np.NaN, np.nan, np.NAN]:
        mcc_white_condition = ""
    else:
        mcc_white_condition = " and mchnt_tp rlike \"" + df['mcc_white'] + "\""
    return " " + col1 + " = \"" + df[type1] + "\" and " + col2 + " = \"" + df[type2] + "\"" + name_white_condition \
           + citycode_white_condition + name_black_condition + mcc_white_condition + " limit " + str(limit)


def merchant_clean_hql(df, new_table, location, from_table, limit=200000000, type1='type1', type2='type2', col1='type1',
                       col2='type2', head=True):
    """
    商户清洗(HQL)
    :param df: DataFrame, 清洗规则表
    :param new_table: str, 新建表名称，用于存放查询结果
    :param location: str, hadoop存储位置
    :param from_table: str, 所查询的表名称（匹配完type的商户名称表）
    :param type1: str, 清洗规则表中type1列名
    :param type2: str, 清洗规则表中type2列名
    :param col1: str, hive数据表相应的type1列名
    :param col2: str, hive数据表相应的type2列名
    :param head: bool, hive脚本开头设置内容
    :return: str, hql
    """
    # mcc, mchnt_name, mchnt_cd, term_id, city_cd
    df['citycode_white'] = df['citycode_white'].astype(str)
    df['mcc_white'] = df['mcc_white'].astype(str)
    drop_tab = "\n\ndrop table if exists " + new_table + ";"
    create_select = "\ncreate table " + new_table + " stored as orcfile location \"" + location + "\" as\nselect distinct a.*"
    sub_table = "select * from " + from_table + " where " + df.apply(merchant_clean_where_condition, axis=1,
                                                                     limit=limit, type1=type1, type2=type2, col1=col1, col2=col2)
    from_tab = "\nfrom\n\t(\n\t" + " union all\n\t".join(sub_table) + "\n\t) a;"
    if head:
        head = "\nset mapreduce.job.queuename=root.Upsmart.Upsmart_queue2;" \
               "\nset mapred.reduce.tasks = 60;" \
               "\nset hive.auto.convert.join = true;" \
               "\nuse default_00010018_zhandy;\n"
        hql = "\n" + head + drop_tab + create_select + from_tab
    else:
        hql = "\n" + drop_tab + create_select + from_tab
    return hql


def transaction_matching_hql(df, new_table, location, from_date='2015-01-01', to_date='2018-05-31',
                             acpt_resp_cd=None, trans_at=None, trans_id=None, trans_status=None, head=True):
    """
    批量生成商户名称及交易流水匹配HQL
    :param df: DataFrame, 含有商户关键字白/黑名单（正则表达式）的品牌对应关键字表
    :param new_table: str, 匹配得到的商户集合表名称
    :param location: str, hadoop存储位置
    :param from_date: str, 流水日期筛选起点，yyyy-mm-dd格式
    :param to_date: str, 流水日期筛选终点，yyyy-mm-dd格式
    :param acpt_resp_cd: str, 应答码字段筛选条件，如“acpt_resp_cd = 00"
    :param trans_at: str, 交易金额字段筛选条件，如”trans_at > 1"
    :param head: bool, hive脚本开头设置内容
    :return: str, hql
    """
    drop_tab = "\n\ndrop table if exists " + new_table + ";"
    create_tab = "\ncreate table " + new_table + " stored as orcfile location '" + location + "' as\nselect"
    select_col = """
    pri_acct_no, mchnt_cd, trans_id, mchnt_tp, acpt_ins_id_cd, 
    trans_chnl, term_id, trans_at, trans_rcv_ts, acpt_resp_cd,
    trans_curr_cd, fwd_settle_curr_cd, fwd_settle_at, mchnt_name,
    trans_status, dt, substr(dt, 1, 7) as year_month,
    ceil(unix_timestamp(concat(dt, ' 00:00:00'))-1419955200/86400/7) as week
    """
    from_tab = "from dw.tbl_fct_trans_sm3"
    dt_condition = "\nwhere  dt between '" + from_date + "' and '" + to_date + "'"
    if acpt_resp_cd:
        acptt_resp_cd_condition = "\n\tand " + acpt_resp_cd
    else:
        acptt_resp_cd_condition = ""

    if trans_at:
        trans_at_condition = "\n\tand " + trans_at
    else:
        trans_at_condition = ""

    if trans_id:  # 交易代码
        trans_id_condition = "\n\tand" + trans_id
    else:
        trans_id_condition = ""

    if trans_status:  # 交易状态
        trans_status_condition = "\n\tand" + trans_status
    else:
        trans_status_condition = ""

    mchnt_name_condition = "\n\tand (\n\t\t" + " or\n\t\t".join(df['where_hql']) + "\n\t\t);"
    where_condition = dt_condition + acptt_resp_cd_condition + trans_at_condition + trans_id_condition \
                      + trans_status_condition + mchnt_name_condition
    if head:
        head = "\nset mapreduce.job.queuename=root.Upsmart.Upsmart_queue2;" \
               "\nset mapred.reduce.tasks = 60;" \
               "\nset hive.auto.convert.join = true;" \
               "\nuse default_00010018_zhandy;\n"
        hql = "\n" + head + drop_tab + create_tab + select_col + from_tab + where_condition
    else:
        hql = "\n" + drop_tab + create_tab + select_col + from_tab + where_condition
    return hql


def save_to_file(file, contents, encoding='utf-8'):
    """
    保存到本地文件
    :param file: str, 本地文件名
    :param contents: str, 待保存的对象
    :param encoding: str, 编码方式
    :return: 本地file文件
    """
    fh = open(file, 'w', encoding=encoding)
    fh.write(contents)
    fh.close()


def read_text(file, columns, sep=',', encoding='gbk', quoting=1):
    """
    导入本地中不带列名的文本文件
    :param file: str, 待导入的文本文件路径
    :param columns: list, 列名
    :param sep: str, 分隔符
    :param quoting: Control field quoting behavior per csv.QUOTE_* constants.
    :return: DataFrame
    """
    df = pd.read_csv(file, sep=sep, encoding=encoding, header=None, quoting=quoting)
    df.columns = columns
    for col in columns:
        if col.lower() in ['mcc', 'mchnt_tp', 'city_code', 'city_cd']:
            df[col] = df[col].apply(lambda x: str(x).rjust(4, "0"))
    return df


def fill_zero(df, column, width=4):
    """
    指定列用"0"向左填充至指定位数
    :param df: DataFrame, 待填充处理的表名
    :param column: str, 待填充处理的列名
    :param width: int, 填充位数要求
    :return: DataFrame
    """
    df2 = df.copy()
    df2[column] = df2[column].apply(lambda x: str(x).rjust(width, "0"))
    return df


def missing_check(check, base):
    missing = check[~check.isin(base.unique())]
    if len(missing) > 0:
        print("\n注意！发现缺失项：\n", missing)
    else:
        print("检查完毕，未发现缺失。")


def type_decode(df, coding, code=['industry', 'district'], decode={'industry': '公司', 'district': '品牌'}):
    """
    对列值进行解码，即把取值为代码的列解码成相应的实际名称，解码结果以新的列追加到原表末尾
    :param df: DataFrame, 含有待解码列的数据表
    :param coding: DataFrame, 编码-解码表
    :param code: list, 需要解码的列名
    :param decode: dict, 编码-解码表中的对应关系，{编码字段名：相应解码字段名}
    :return: DataFrame
    """
    if len(code) != len(decode):
        print("\n编码与解码字段个数不匹配，请重新输入\n")
        return df
    decode_key = list(decode.keys())
    decode_value = list(decode.values())
    # 检查唯一性
    coding_tmp = coding[decode_key + decode_value].drop_duplicates()

    check_code = coding_tmp.groupby(decode_key)[decode_value].count().reset_index()
    check_code = check_code[check_code[decode_value].sum(1) > len(decode_value)]
    if len(check_code) > 0:
        print("\n编码与解码为一对多，不满住唯一性，请检查:\n")
        print(check_code)

    check_decode = coding_tmp.groupby(decode_value)[decode_key].count().reset_index()
    check_decode = check_decode[check_decode[decode_key].sum(1) > len(decode_key)]
    if len(check_decode) > 0:
        print("\n解码与编码值为一对多，不满住唯一性，请检查:\n")
        print(check_decode)

    # 开始解码
    coding_tmp.columns = code + decode_value
    df2 = df.merge(coding_tmp, how='left', on=code)
    df_check = df2[code + decode_value].drop_duplicates()
    df_check = df_check[df_check.isna().sum(1) > 0]
    if len(df_check) > 0:
        print("\n注意，存在解码失败，请检查：\n")
        print(df_check)
    return df2


def excels_title_decode(path, coding, path_output=None, show=True):
    """
    对指定路径中的所有Excel工作簿名及其工作表的名称进行解码, 输出到原路径下的output文件夹里
    :param path: str, 待解码的Excel文件所在路径
    :param coding: DataFrame, 编码对应表
    :param path_output: str, 解码后的Excel文件输出路径
    :param show: boolean, 是否打印过程
    """
    if not path_output:
        path_output = path + 'output/'
    make_directory(path_output)
    excels = [f for f in os.listdir(path) if os.path.isfile(os.path.join(path, f)) and (f[-4:] == 'xlsx')]
    for file in excels:
        company_code = file[:-5]  # 公司代码
        company_name = coding[coding['公司代码'] == company_code]['公司'].iloc[0]  # 公司名称
        if show:
            print(company_code, ': ', company_name)
        output_file = path_output + company_name + '.xlsx'  # 相应的解码名称后的文件
        pd.DataFrame().to_excel(output_file, index=False)  # 在输出路径初实化/新建一个相应的文件
        writer = pd.ExcelWriter(output_file)
        reader = pd.ExcelFile(os.path.join(path, file))
        # 遍历Excel工作簿中的所有工作表，工作表名即为品牌代码
        for sheet in reader.sheet_names:
            brand_name = coding[(coding['公司代码'] == company_code)&(coding['品牌代码'] == sheet)]['品牌'].iloc[0]  # 品牌名称
            if show:
                print('\t', sheet, '--', brand_name)
            df = pd.read_excel(reader, sheet_name=sheet)
            df.to_excel(writer, sheet_name=brand_name)
            writer.save()
            writer.close()
            reader.close()


def type_coding(df, coding, company='公司', brand='品牌', type1='type1', type2='type2'):
    """
    对列值进行编码，即把取值为公司、品牌名称的列编码成相应的公司及品牌代码，编码结果以新的列追加到原表末尾
    :param df: DataFrame, 含有待编码列的数据表
    :param coding: DataFrame, 编码-解码表
    :param company: str, 公司名称所在列名
    :param brand: str, 品牌名称所在列名
    :param type1: str, 公司代码所在列名
    :param type2: str, 品牌代码所在列名
    :return: DataFrame
    """
    coding2 = coding[['公司', '公司代码', '品牌', '品牌代码']]\
        .rename(columns={'公司': company, '公司代码': type1, '品牌': brand, '品牌代码': type2})
    df2 = df.merge(coding2, 'left', [company, brand])
    df_check = df2[[company, brand, type1, type2]].drop_duplicates()
    df_check = df_check[df_check[[company, brand, type1, type2]].isna().sum(1) > 0]
    if len(df_check) > 0:
        print("\n注意，存在编码失败：\n")
        print(df_check)
    return df2


def df_to_excels_v0(df, path_output=None, company='公司', brand='品牌', show=True):
    """
    基于公司及品牌拆分数据表，每个公司一个Excel工作簿，同个公司旗下的每个品牌一个sheet
    :param df: DataFrame, 待拆分的数据表
    :param path_output: str, 生成的Excel保存的路径
    :param company: str, 公司字段名
    :param brand: str, 品牌字段名
    :param show: boolean, 是否打印过程
    :return: 本地Excel文件
    """
    if not path_output:
        path_output = './output/'
    make_directory(path_output)
    # 两层循环，第一层基于公司循环生成Excel工作簿
    for comp in df[company].drop_duplicates():
        output_file = path_output + str(comp) + '.xlsx'
        pd.DataFrame().to_excel(output_file, index=False)
        writer = pd.ExcelWriter(output_file)
        df_company = df[df[company] == comp]
        if show:
            print(comp)
        # 第二层基于品牌循环生成sheet工作表，保存到相应公司的工作簿中
        for bran in df_company[brand].drop_duplicates():
            df_brand = df_company[df_company[brand] == bran]
            df_brand.to_excel(writer, index=False, sheet_name=str(bran))
            if show:
                print('\t', bran)
        writer.save()
        writer.close()


def df_to_excels(df, excel_name, sheet_name=None, path_output=None, show=True):
    """
    基于指定的列值将数据拆分保存成Excel
    :param df: DataFrame, 待拆分的数据表
    :param excel_name: str, excel名称字段名
    :param sheet_name: str, sheet名称字段名
    :param path_output: str, 生成的Excel保存的路径
    :param show: boolean, 是否打印过程
    :return: 本地Excel文件
    """
    if not path_output:
        path_output = './output/'
    make_directory(path_output)
    # 两层循环，第一层基于公司循环生成Excel工作簿
    for comp in df[excel_name].drop_duplicates():
        output_file = path_output + str(comp) + '.xlsx'
        pd.DataFrame().to_excel(output_file, index=False)
        writer = pd.ExcelWriter(output_file)
        df_excel = df[df[excel_name] == comp]
        if show:
            print(comp)
        if sheet_name:
            # 第二层基于品牌循环生成sheet工作表，保存到相应公司的工作簿中
            for bran in df_excel[sheet_name].drop_duplicates():
                df_sheet = df_excel[df_excel[sheet_name] == bran]
                df_sheet.to_excel(writer, index=False, sheet_name=str(bran))
            if show:
                print('\t', bran)
        else:
            df_excel.to_excel(writer, index=False, sheet_name=str(comp))
        writer.save()
        writer.close()


def upper_letter_check(df, columns=['name_white', 'name_black'], primary_column=['company', 'brand']):
    """
    正则表达式检查是否存在大写字母
    :param df: DataFrame, 待检查的数据表
    :param columns: list, 需要执行正则表达式检查的列名
    :param primary_column: list, 具有唯一标识性的列名
    :return: None
    """
    def upper_letter_filter(x):
        x = str(x)
        if re.search('[A-Z]', x):
            return True
        else:
            return False
    df2 = df.copy()
    check_bool = df2[columns].applymap(upper_letter_filter)
    df3 = df2[primary_column + columns][check_bool.sum(1) > 0]
    if len(df3):
        print("\n下列规则的正则表达式存在大写字母")
        print(df3)
    else:
        print("\n检查完毕，没有发现大写字母")


def str_lower(df, columns):
    """
    取值全部转成小写
    :param df: DataFrame, 待检查的数据表
    :param columns: list, 列取值需要转成小写的列名
    :return: DataFrame
    """
    df2 = df.copy()
    df2[columns] = df2[columns].applymap(lambda x: str(x).lower())
    return df2


def or_pattern_check(df, columns=['name_white', 'citycode_white', 'name_black', 'mcc_white'],
                     primary_column=['company', 'brand'], single=False):
    """
    检查是否存在多余的"|"
    :param df: DataFrame, 待检查的数据表
    :param columns: list, 需要执行正则表达式检查的列名
    :param primary_column: list, 具有唯一标识性的列名
    :param single, bool
    :return: None
    """
    def or_pattern_filter(x):
        x = str(x)
        if re.search('^\|.+', x) or re.search('.+\|$', x) or re.search('\|\|', x) or re.search('\|\n\|', x):
            return True
        else:
            return False

    def single_or_pattern_filter(x):
        x = str(x)
        if re.search('^\|.+', x) or re.search('.+\|$', x) or re.search('\|\|', x) or re.search('\|\n\|', x) or re.search('^\|$', x):
            return True
        else:
            return False
    df2 = df.copy()
    if single:
        check_bool = df2[columns].applymap(single_or_pattern_filter)
    else:
        check_bool = df2[columns].applymap(or_pattern_filter)
    df3 = df2[primary_column + columns][check_bool.sum(1) > 0]
    if len(df3):
        print("\n下列规则的正则表达式可能存在多余的'|':\n")
        print(df3)
    else:
        print("\n检查完毕，没有发现多余的'|'\n")


def bracket_pattern_check(df, columns=['name_white', 'citycode_white', 'name_black', 'mcc_white'],
                          primary_column=['company', 'brand']):
    """
    检查是否存在多余的"|"
    :param df: DataFrame, 待检查的数据表
    :param columns: list, 需要执行正则表达式检查的列名
    :param primary_column: list, 具有唯一标识性的列名
    :return: None
    """
    def bracket_pattern_filter(x):
        x = str(x)
        if re.search('）', x) or re.search('（', x):
            return True
        else:
            return False
    df2 = df.copy()
    check_bool = df2[columns].applymap(bracket_pattern_filter)
    df3 = df2[primary_column + columns][check_bool.sum(1) > 0]
    if len(df3):
        print("\n下列规则的正则表达式可能存在中文括号:\n")
        print(df3)
    else:
        print("\n检查完毕，没有发现中文括号。\n")


def find_escape_character(df, columns=['name_white', 'name_black']):
    """
    查找带有转义字符的正则表达式（结果去重）
    :param df: DataFrame
    :param columns: list
    :return: set
    """
    dic_escape = dict()
    set_escape = set()
    for col in columns:
        s_escape = df[col].astype(str).str.findall(r"\\.{1}")[df[col].astype(str).str.contains(r"\\.{1}")]
        dic_escape[col] = []
        for i in s_escape:
            dic_escape[col] = dic_escape[col] + i
        dic_escape[col] = list(set(dic_escape[col]))
        set_escape = set_escape|set(dic_escape[col])
    print("各字段中含有的转义字符分别为:\n\t%s"%dic_escape)
    print("所有字段含有的转义字符（去重）为:\n\t%s"%set_escape)


def company_file_rule_check(rule, path):
    """
    检查是否有公司缺少清洗规则
    :param rule: DataFrame, 清洗规则表
    :param path: str, 以公司名称命名的Excel文件所在路径
    :return: (print)
    """
    excel_files = pd.Series([f[:-5] for f in os.listdir(path) if os.path.isfile(os.path.join(path, f)) and (f[-4:] == 'xlsx')])
    check = excel_files[~excel_files.isin(rule['company'].unique())]
    if len(check) > 0:
        print("\n注意！下列Excel文件名与清洗规则表的公司名未匹配上\n", check)
    else:
        print("检查完毕，待清洗的公司文档集合中未发现有公司缺清洗规则")


def merchant_clean(df, path, path_clean=None, path_black=None, check=True, show=True,
                   excel_mcc='./MTC-MCC--数据字典.xlsx', excel_city_cd='./银联地区代码对应表（201708）v1_revised.xlsx',
                   columns_clean={'mchnt_name': 'mchnt_name', 'city_cd': 'city_cd', 'mcc': 'mcc'}):
    """
    商户文档清洗
    :param df: DataFrame, 清洗规则表
    :param path: str, 商户文档路径
    :param path_clean: str, 清洗文档（保留商户）输出路径
    :param path_black: str, 清洗文档（剔除商户）输出路径
    :param check: boolean, 是否检查待清洗的品牌在清洗规则表中
    :param show: boolean, 是否打印过程
    :param excel_mcc: str, MTC-MCC--数据字典.xlsx, 内含MTC-MCC对应关系工作表
    :param excel_city_cd: str, 银联地区代码对应表（201708）v1_revised.xlsx
    :param columns_clean: dict{str: str}, 执行清洗的列名字典，内容{指定列名：实际列名}
    """
    df = df.astype('object')
    # 清洗结果输出路径准备
    if not path_clean:
        path_clean = path + "clean/"
    make_directory(path_clean)
    if not path_black:
        path_black = path + "black/"
    make_directory(path_black)
    if os.path.exists(excel_mcc):
        df_key_mcc = pd.read_excel(excel_mcc, 'MTC-MCC对应关系')
        df_key_mcc['MCC'] = df_key_mcc['MCC'].apply(lambda x: str(x).rjust(4, "0"))
    else:
        print('找不到%s' % excel_mcc)
    if os.path.exists(excel_city_cd):
        df_key_city_code = pd.read_excel(excel_city_cd)
        df_key_city_code['district_no'] = df_key_city_code['district_no'].apply(lambda x: str(x).rjust(4, "0"))
    else:
        print('找不到%s' % excel_city_cd)

    # 三层循环，第一层遍历公司，第二层遍历公司旗下的品牌，第三层遍历执行每个品牌的清洗规则
    companys_excel = [f[:-5] for f in os.listdir(path) if os.path.isfile(os.path.join(path, f)) and (f[-4:] == 'xlsx')]
    companys_rule = df['company'].unique()
    for company in companys_excel:
        if check:  # 检查待清洗的公司是否存在相应的清洗规则
            if company not in companys_rule:
                print("\n%s\t不在清洗规则表中, 清洗跳过" % company)
                continue
        print("\n%s\t%s" % (company, time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())))
        # 输出文件初始化
        file_clean = path_clean + company + '_clean.xlsx'
        file_black = path_black + company + '_black.xlsx'
        writer_clean = pd.ExcelWriter(file_clean)
        writer_black = pd.ExcelWriter(file_black)

        reader_raw = pd.ExcelFile(path + company + '.xlsx')

        df_rule_company = df[(df['company'] == company)]  # 公司相应的清洗规则

        brands_excel = reader_raw.sheet_names
        brands_rule = df_rule_company['brand'].unique()
        for brand in brands_excel:
            if check:  # 检查待清洗的品牌是否存在相应的清洗规则
                if brand not in brands_rule:
                    print("\t%s: 不在清洗规则表中, 清洗跳过" % brand)
                    continue
            # 读入待清洗品牌的商户集合数据
            df_raw = pd.read_excel(reader_raw, brand, dtype='str')
            df_raw[columns_clean['city_cd']] = df_raw[columns_clean['city_cd']].apply(lambda x: str(x).rjust(4, "0"))
            df_raw[columns_clean['mcc']] = df_raw[columns_clean['mcc']].apply(lambda x: str(x).rjust(4, "0"))
            if os.path.exists(excel_mcc):
                df_raw = df_raw.merge(df_key_mcc[['MCC', 'MCC名称']].rename(
                    columns={'MCC': columns_clean['mcc'], 'MCC名称': 'mcc名称'}
                ), 'left', columns_clean['mcc'])
            if os.path.exists(excel_city_cd):
                df_raw = df_raw.merge(df_key_city_code[['district_no', 'city']].rename(
                    columns={'district_no': columns_clean['city_cd'], 'city': '所在地'}
                ), 'left', columns_clean['city_cd'])
            count_raw = len(df_raw)
            df_rule_brand = df_rule_company[df_rule_company['brand'] == brand]  # 品牌相应的清洗规则

            # 输出表格初始化准备
            df_clean = pd.DataFrame()
            df_black = pd.DataFrame()

            # 第三层循环，遍历执行相应的清洗规则（每个品牌可能有一个或多个清洗规则）
            for j in range(len(df_rule_brand)):
                # 规则一：商户名称白名单
                name_white = df_rule_brand['name_white'].iloc[j]
                df_name_white = df_raw[df_raw[columns_clean['mchnt_name']].str.contains(name_white,
                                                                                        flags=re.IGNORECASE)]
                df_name_white['keywords'] = name_white
                df_raw.drop(df_name_white.index, inplace=True)  # 剔除已匹配名称白名单的，剩下未匹配的

                # 规则二：城市白名单
                city_code_white = df_rule_brand['citycode_white'].iloc[j]
                df_city_code = df_name_white[df_name_white[columns_clean['city_cd']].str.contains(city_code_white,
                                                                                                  flags=re.IGNORECASE)]
                df_name_white.drop(df_city_code.index, inplace=True)
                if len(df_name_white) > 0:
                    df_name_white['drop_reason'] = '不在citycode_white内'
                    df_black = df_black.append(df_name_white)

                # 规则三：商户名称黑名单
                name_black = df_rule_brand['name_black'].iloc[j]
                df_name_black = df_city_code[~df_city_code[columns_clean['mchnt_name']].str.contains(name_black,
                                                                                                     flags=re.IGNORECASE)]
                df_city_code.drop(df_name_black.index, inplace=True)
                if len(df_city_code) > 0:
                    df_city_code['drop_reason'] = 'name_black'
                    df_black = df_black.append(df_city_code)

                # 规则四：商户类型
                mcc_white = df_rule_brand['mcc_white'].iloc[j]
                df_mcc_white = df_name_black[df_name_black[columns_clean['mcc']].str.contains(mcc_white,
                                                                                              flags=re.IGNORECASE)]
                df_name_black.drop(df_mcc_white.index, inplace=True)
                if len(df_name_black) > 0:
                    df_name_black['drop_reason'] = 'MCC 不在范围内'
                    df_black = df_black.append(df_name_black)
                df_clean = df_clean.append(df_mcc_white)

            count_clean = len(df_clean)
            count_black = len(df_black)
            count_unmatch = len(df_raw)
            if show:
                print("\t%s: 共%d行，其中clean:black:unmatch = %.1f%% : %.1f%% : %.1f%% = %d : %d : %d"
                      % (brand, count_raw, (100*count_clean/count_raw), (100*count_black/count_raw),
                         (100*count_unmatch/count_raw), count_clean, count_black, count_unmatch))

            if count_clean > 0:
                df_clean.to_excel(writer_clean, sheet_name=brand, index=None)
            if count_black > 0:
                df_black.to_excel(writer_black, sheet_name=brand, index=None)
            if count_unmatch > 0:
                df_raw.to_excel(writer_black, sheet_name=brand + '_未匹配', index=None)
        try:
            writer_clean.save()
            writer_clean.close()
            writer_black.save()
            writer_black.close()
        except Exception:
            print('--------------出错了----------------')
            print('traceback.print_exc():')
            print(traceback.print_exc())


def merchant_clean_v0(df, path, path_clean=None, path_black=None, check=True,
    columns_clean={'mchnt_name': 'mchnt_name', 'city_cd': 'city_cd', 'mcc': 'mcc'}, show=True):
    """
    商户文档清洗
    :param df: DataFrame, 清洗规则表
    :param path: str, 商户文档路径
    :param path_clean: str, 清洗文档（保留商户）输出路径
    :param path_black: str, 清洗文档（剔除商户）输出路径
    :param columns_clean: dict{str: str}, 执行清洗的列名字典，内容{指定列名：实际列名}
    :param check: boolean, 是否检查待清洗的品牌在清洗规则表中
    :param show: boolean, 是否打印过程
    """
    # 清洗结果输出路径准备
    if not path_clean:
        path_clean = path + "clean/"
    make_directory(path_clean)
    if not path_black:
        path_black = path + "black/"
    make_directory(path_black)

    # 三层循环，第一层遍历公司，第二层遍历公司旗下的品牌，第三层遍历执行每个品牌的清洗规则
    companys_excel = [f[:-5] for f in os.listdir(path) if os.path.isfile(os.path.join(path, f)) and (f[-4:] == 'xlsx')]
    companys_rule = df['company'].unique()
    for company in companys_excel:
        if check:  # 检查待清洗的公司是否存在相应的清洗规则
            if company not in companys_rule:
                print("\n%s\t不在清洗规则表中, 清洗跳过" % company)
                continue
        print("\n%s\t%s" % (company, time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())))
        # 输出文件初始化
        file_clean = path_clean + company + '_clean.xlsx'
        file_black = path_black + company + '_black.xlsx'
        writer_clean = pd.ExcelWriter(file_clean)
        writer_black = pd.ExcelWriter(file_black)

        reader_raw = pd.ExcelFile(path + company + '.xlsx')

        df_rule_company = df[(df['company'] == company)]  # 公司相应的清洗规则

        brands_excel = reader_raw.sheet_names
        brands_rule = df_rule_company['brand'].unique()
        for brand in brands_excel:
            if check:  # 检查待清洗的品牌是否存在相应的清洗规则
                if brand not in brands_rule:
                    print("\t%s: 不在清洗规则表中, 清洗跳过" % brand)
                    continue
            # 读入待清洗品牌的商户集合数据
            df_raw = pd.read_excel(reader_raw, brand)
            count_raw = len(df_raw)
            df_raw[columns_clean['city_cd']] = df_raw[columns_clean['city_cd']].apply(lambda x: str(x).rjust(4, "0"))
            df_raw[columns_clean['mcc']] = df_raw[columns_clean['mcc']].apply(lambda x: str(x).rjust(4, "0"))
            df_rule_brand = df_rule_company[df_rule_company['brand'] == brand]  # 品牌相应的清洗规则

            # 输出表格初始化准备
            df_clean = pd.DataFrame()
            df_black = pd.DataFrame()

            # 第三层循环，遍历执行相应的清洗规则（每个品牌可能有一个或多个清洗规则）
            for j in range(len(df_rule_brand)):
                # 规则一：商户名称白名单
                name_white = df_rule_brand['name_white'].iloc[j]
                df_name_white = df_raw[df_raw[columns_clean['mchnt_name']].str.contains(name_white,
                                                                                        flags=re.IGNORECASE)]
                df_name_white['keywords'] = name_white
                df_raw.drop(df_name_white.index, inplace=True)  # 剔除已匹配名称白名单的，剩下未匹配的

                # 规则二：城市白名单
                city_code_white = df_rule_brand['citycode_white'].iloc[j]
                df_city_code = df_name_white[df_name_white[columns_clean['city_cd']].str.contains(city_code_white,
                                                                                                  flags=re.IGNORECASE)]
                df_name_white.drop(df_city_code.index, inplace=True)
                if len(df_name_white) > 0:
                    df_name_white['drop_reason'] = '不在citycode_white内'
                    df_black = df_black.append(df_name_white)

                # 规则三：商户名称黑名单
                name_black = df_rule_brand['name_black'].iloc[j]
                df_name_black = df_city_code[~df_city_code[columns_clean['mchnt_name']].str.contains(name_black,
                                                                                                     flags=re.IGNORECASE)]
                df_city_code.drop(df_name_black.index, inplace=True)
                if len(df_city_code) > 0:
                    df_city_code['drop_reason'] = 'name_black'
                    df_black = df_black.append(df_city_code)

                # 规则四：商户类型
                mcc_white = df_rule_brand['mcc_white'].iloc[j]
                df_mcc_white = df_name_black[df_name_black[columns_clean['mcc']].str.contains(mcc_white,
                                                                                              flags=re.IGNORECASE)]
                df_name_black.drop(df_mcc_white.index, inplace=True)
                if len(df_name_black) > 0:
                    df_name_black['drop_reason'] = 'MCC 不在范围内'
                    df_black = df_black.append(df_name_black)
                df_clean = df_clean.append(df_mcc_white)

            count_clean = len(df_clean)
            count_black = len(df_black)
            count_unmatch = len(df_raw)
            if show:
                print("\t%s: 共%d行，其中clean:black:unmatch = %.1f%% : %.1f%% : %.1f%% = %d : %d : %d"
                      % (brand, count_raw, (100*count_clean/count_raw), (100*count_black/count_raw),
                         (100*count_unmatch/count_raw), count_clean, count_black, count_unmatch))
            if count_clean > 0:
                df_clean.to_excel(writer_clean, sheet_name=brand, index=None)
            if count_black > 0:
                df_black.to_excel(writer_black, sheet_name=brand, index=None)
            if count_unmatch > 0:
                df_raw.to_excel(writer_black, sheet_name=brand + '_未匹配', index=None)
        try:
            writer_clean.save()
            writer_clean.close()
            writer_black.save()
            writer_black.close()
        except Exception:
            print('--------------出错了----------------')
            print('traceback.print_exc():')
            print(traceback.print_exc())


def excel_to_df(excel, col_type='str', show=True):
    """
    将excel文件中的所有sheet工作表合并成一个DataFrame
    :param excel: str, 含有绝对路径及文件后缀的Excel文件，如d:/folder_name/file_name.xlsx
    :param col_type: str or dict, 各字段数据类型
    :param show: boolean, 是否打印过程
    :return: DataFrame
    """
    xlsx = pd.ExcelFile(excel)
    sheet_names = xlsx.sheet_names
    df = pd.DataFrame()
    for sheet in sheet_names:
        df = df.append(pd.read_excel(xlsx, sheet_name=sheet, dtype=col_type))
        if show:
            print('\t', sheet)
    return df


def excels_to_df(path, col_type='str', show=True):
    """
    将路径中所有excel文件的所有sheet工作表合并成一个DataFrame
    :param path: str, Excel文件所在路径，如d:/folder_name/
    :param col_type: str or dict, 各字段数据类型
    :param show: boolean, 是否打印过程
    :return: DataFrame
    """
    # 两层循环，第一层遍历Excel工作簿，第二层遍历工作簿里面的sheet
    df = pd.DataFrame()
    excels = [f for f in os.listdir(path) if os.path.isfile(os.path.join(path, f)) and (f[-4:] == 'xlsx')]
    for excel in excels:
        if show:
            print(excel)
        excel_file = pd.ExcelFile(path + excel)
        sheet_names = excel_file.sheet_names
        for sheet in sheet_names:
            if show:
                print('\t', sheet)
            df = df.append(pd.read_excel(excel_file, sheet_name=sheet, dtype=col_type))
    return df


def format_adjust(df, base=['公司', '品牌'], transpose='品牌', sep="|", name=None):
    """
    列装置（针对汽车行业数据处理）
    :param df: DataFrame, 数据表
    :param base: list, 基础列列名
    :param transpose: str, 需要转置列的列名
    :param sep : str, 分隔符
    :param name : str, 装置后的列名
    :return: DataFrame
    """
    df2 = df[list(set(base + [transpose]))].groupby(base).apply(lambda x: sep .join(x[transpose]))
    df2 = df2.reset_index()
    if not name:
        df2.columns = base + [transpose]
    else:
        df2.columns = base + [name]
    return df2


def statistic_monthly(df, brand_range, date='日期', left_on=['公司', '品牌'], right_on=['公司', '品牌'],
                  statistic=['金额', '笔数', '卡数', 'mid', 'pid'], keep=True):
    """
    交易流水筛选汇总：月度-->月度
    :param df: DataFrame, 原始流水统计表
    :param brand_range:  DataFrame, 品牌对应关键字表
    :param date: str, df表中的日期字段名，数据类型为datetime.date
    :param left_on:  list, 左表（df)连接键
    :param right_on:  list, 右表(brand_range)连接键
    :param statistic:  list, 需要进行统计处理的指标
    :param keep: boolean, 是否输出合并前的品牌数据
    :return: [DataFrame, DataFrame] 品牌层面及公司层面的流水统计表
    """
    # 表格合并
    df.sort_values(left_on + [date], inplace=True)
    tmp = df.merge(
        brand_range[right_on + ['品牌纳入日期', '品牌剔除日期', '日期待定标识']],
        how='left',
        left_on=left_on,
        right_on=right_on)
    # 品牌日期范围筛选
    tmp['品牌纳入日期'].fillna(value=0, inplace=True)
    tmp['品牌剔除日期'].fillna(value=29999999, inplace=True)
    tmp['date_int'] = tmp[date].apply(lambda x: x.year * 10000 + x.month * 100 + x.day)  # 日期转成整型表示
    df_select = tmp[(tmp['date_int'] >= tmp['品牌纳入日期']) & (tmp['date_int'] < tmp['品牌剔除日期'])]
    # 品牌汇总
    if keep:
        brand_statistic = df[left_on + [date] + statistic]
    else:
        brand_statistic = df_select[left_on + [date] + statistic]
    brand_statistic.index = range(len(brand_statistic))
    # 公司汇总
    company_statistic = df_select.groupby([left_on[0], date])[statistic].sum()
    company_statistic = company_statistic.reset_index()
    company_statistic.insert(1, left_on[1], company_statistic[left_on[0]].apply(lambda x: x + '_合并'))
    return brand_statistic, company_statistic


def year_month_to_date(df, year='year', month='month'):
    """
    将年和月两列合并成yyyy-mm-dd格式的日期
    :param df: DataFrame, 数据表
    :param year: str, 年份所在的列名
    :param month: str, 月份所在的列名
    :return: Series
    """
    date = df[year].astype(str) + df[month].apply(lambda x: str(x).rjust(2, '0'))
    return date.apply(lambda x: dt.datetime.strptime(x, "%Y%m").date())


def year_week_to_date(df, year='年', week='周'):
    """
    将年和周两列合并成yyyy-mm-dd格式的日期
    :param df: DataFrame
    :param year: df表中的年份字段名
    :param week: df表中的周序字段名，培训方法为每年的1月1日起，每7天为一周
    :return:
    """
    return df[[year, week]].apply(lambda x:  dt.date(int(x[year]), 1, 1) + dt.timedelta(7*(int(x[week]) - 1)), 1)


def statistic_weekly(df, brand_range, year='年', week='周', left_on=['公司', '品牌'], right_on=['公司', '品牌'],
                  statistic=['金额', '笔数', '卡数', 'mid', 'pid'], keep=True):
    """
    交易流水筛选汇总：周度-->周度
    :param df: DataFrame, 原始流水统计表
    :param brand_range:  DataFrame, 品牌对应关键字表
    :param year: str, df表中的年份字段名
    :param year: str, df表中的周序字段名，培训方法为每年的1月1日起，每7天为一周
    :param left_on:  list, 左表（df)连接键
    :param right_on:  list, 右表(brand_range)连接键
    :param statistic:  list, 需要进行统计处理的指标
    :param keep: boolean, 是否输出合并前的品牌数据
    :return: [DataFrame, DataFrame] 品牌层面及公司层面的流水统计表
    """
    df2 = df.copy()
    df2['日期'] = year_week_to_date(df, year=year, week=week)
    brand_statistic, company_statistic = statistic_monthly(df=df2, brand_range=brand_range, left_on=left_on,
                                                           right_on=right_on, statistic=statistic, keep=keep)
    return brand_statistic, company_statistic


def get_quarter(df, date='日期'):
    """
    将yyyy-mm-dd格式的日期转成yyyyqq格式的季度
    :param df: DataFrame
    :param date: str, 日期所在的列名，列值必须是datetime.date类型
    :return: Series
    """
    return df[date].apply(lambda x: str(x.year)) + df[date].apply(lambda x: str((x.month-1)//3 + 1).rjust(2, 'Q'))


def get_period(df, df_key, company_data='公司', company_key='公司', period_start='财报周期起始月份', date='日期'):
    """
    根据周期起始日期划分财报周期
    :param df: DataFrame
    :param df_key: DataFrame
    :param company_data: str
    :param company_key: str
    :param period_start: int
    :param date: date
    :return:
    """
    df_key2 = df_key[[company_key, period_start]].copy()
    company_check = [i for i in df[company_data].unique() if i not in df_key2[company_key].unique()]
    if len(company_check) > 0:
        print("以下公司的财报周期起始月份缺少记录\n\t%s"%company_check)
        df_key2 = df_key2.append(pd.DataFrame({company_key: company_check, period_start: 1}))
    df_key2 = df_key2[df_key2[company_key].isin(df[company_data].unique())].sort_values([company_key, period_start])
    na_check = df_key2.groupby(company_key).count()
    na_check = na_check[na_check[period_start] == 0]
    if len(na_check) > 0:
        print("以下公司的财报周期起始月份为空值：\n\t%s"%na_check.index.values)
        df_key2.loc[df_key2[company_key].isin(na_check.index.values), [period_start]] = 1
    df_key2.fillna(method='ffill', inplace=True)
    value_check = df_key2.groupby(company_key).max().merge(df_key2.groupby(company_key).mean(), 'left', left_index=True, right_index=True)
    value_check = value_check[value_check[period_start + '_x'] != value_check[period_start + '_y']]
    if len(value_check) > 0:
        print("以下公司有多个不同的财报周期起始月份：\n\t%s"%value_check.index.values)
    df_period_start = df_key2.groupby(company_key).first()
    df2 = df.merge(df_period_start, 'left', left_on=company_data, right_index=True)
    df2[period_start] = df2[period_start].astype(int)

    def period_get(x, d=date, s=period_start):
        if x[d].month < x[s]:
            return str(x[d].year - 1) + str((x[d].month + 12 - x[s])//3 + 1).rjust(2, 'p')
        else:
            return str(x[d].year) + str((x[d].month - x[s])//3 + 1).rjust(2, 'p')
    return df2[[date, period_start]].apply(period_get, 1)


def statistic_merge(df, group=['公司', '季度'], statistic=['金额', '笔数', '卡数', 'mid', 'pid']):
    """
    聚合统计
    :param df: DataFrame, 数据表
    :param group: list[str], 聚合统计的基础粒度，默认对公司在季度上做聚合
    :param statistic: list[str], 聚合统计的统计指标，默认常见的交易指标
    :return: DataFrame
    """
    df2 = df.groupby(group)[statistic].sum()
    for i in range(len(group)):
        df2.insert(i, group[i], df2.index.get_level_values(group[i]).values)
    df2.index = range(len(df2))
    return df2


def data_masking(df, masking={'金额': 10000, '笔数': 100, '卡数': 100, 'mid': 10, 'pid': 10}):
    """
    对数据表指定列进行相应数量级的脱敏处理
    :param df: DataFrame, 待脱敏的数据表
    :param masking: dic, 待脱敏的列名及相应的脱颖数量级
    :return: DataFrame
    """
    dic_masking = {10: '(十位脱敏)', 100: '(百位脱敏)', 1000: '(千位脱敏)', 10000: '(万位脱敏)', 100000: '(十万位脱敏)',
                   1000000: '(百万位脱敏)', 10000000: '(千万位脱敏)', 100000000: '(亿位脱敏)', 1000000000: '(十亿位脱敏)', }
    columns = list(masking.keys())
    levels = list(masking.values())
    df_masking = df.copy()
    df_masking.drop(columns=columns, inplace=True)
    for i in range(len(columns)):
        column = columns[i]
        level = levels[i]
        df_masking[column + dic_masking[level]] = level * ((df[column]/level).round(0))
    return df_masking


def excels_masking(path, path_output=None, masking={'金额': 10000, '笔数': 100, '卡数': 100, 'mid': 10, 'pid': 10}, show=True, dtype='object'):
    """
    将路径中所有excel文件的所有sheet工作表的指定列进行相应数量级的数据脱敏
    :param path: str, 含有绝对路径及文件后缀的Excel文件，如d:/folder_name/file_name.xlsx
    :param path_output: str, 脱敏数据输出保存路径
    :return: masking: dic, 待脱敏的列名及相应的脱颖数量级
    :param show: boolean, 是否打印过程
    :param dtype: str or dict, 导入数据表字段数据类型说明
    """
    # 两层循环，第一层遍历Excel工作簿，第二层遍历工作簿里面的sheet, 对数据表进行脱敏
    if not path_output:
        path_output = path + 'output/'
    make_directory(path_output)
    excels = [f for f in os.listdir(path) if os.path.isfile(os.path.join(path, f)) and (f[-4:] == 'xlsx')]
    for excel in excels:
        if show:
            print(excel)
        reader = pd.ExcelFile(path + excel)
        writer = pd.ExcelWriter(path_output + excel)
        sheet_names = reader.sheet_names
        for sheet in sheet_names:
            if show:
                print('\t', sheet)
            df_masking = data_masking(pd.read_excel(reader, sheet_name=sheet, dtype=dtype), masking=masking)
            df_masking.to_excel(writer, index=False, sheet_name=sheet)
        writer.save()
        writer.close()


def count_line(file, encoding='utf-8', m=10, show=True):
    """
    计算文件有多少行数据
    :param file: str, 待抽样的文件名，含路径及后缀
    :param encoding: str, 编码方式
    :param m: int, 每次读入处理的数据量，单位为兆
    :param show: bool, 是否打印中间过程
    :return: int, 行数，统计结果
    """
    f = open(file, 'r', encoding=encoding)
    count = 0
    while True:
        chunk = f.read(1024 * 1024 * m)  # 每次读入part M
        if not chunk:
            break
        count += chunk.count('\n')
    f.close()
    if show:
        print("# 该文件共有%d行 #" % count)
    return count


def line_sample(file, encoding=None, m=20, n=5000):
    """
    行抽样
    :param file: str, 待抽样的文件名，含路径及后缀
    :param encoding: str, 编码方式
    :param m: int, 每次读入处理的数据量，单位为兆
    :param n: int, 抽样数量
    :return: 抽样结果，本地文件
    """
    # 提取文件路径及文件名
    file_name = str(os.path.basename(file).split('.')[0])
    file_path = os.path.dirname(file) + '/'

    # 计算文件的总行数
    count = 0
    f = open(file, 'r', encoding=encoding)
    while True:
        chunk = f.read(1024 * 1024 * m)  # 每次读入m M
        if not chunk:
            break
        count += chunk.count('\n')
    f.close()

    # 情况一：样本总数不多于抽样个数时，直接返回全部样本
    if count <= n:
        print("\n%s: 抽样数%d ≥ 总行数%d，无需抽样" % (file_name, n, count))
        return
    else:
        print("\n%s: 共有%d行，约%.1fM，开始抽样..." % (file_name, count, os.path.getsize(file)/1024/1024))
    # 情况二：样本总数超过抽样个数时，基于随机序号进行抽样
    int_range = [i for i in range(count)]  # 产生序号
    random.shuffle(int_range)  # 打散序号
    sample_range = sorted(int_range[:n])  # 抽取前n个随机样本序号

    # 遍历文件，每次读取一部分，基于随机样本序号进行抽样
    i = 0  # 块数计数
    j = 0  # 行数计数
    tail = ''  # 分块残留数据尾部
    df_sample = pd.DataFrame()  # 用于存放抽样结果
    f = open(file, 'r', encoding=encoding)
    while True:
        read = f.read(1024 * 1024 * m)
        if j >= count or not read:
            break
        i += 1
        print("%s\t处理%s第%d部分" % (dt.datetime.now().strftime('%Y-%m-%d %H:%M:%S'), file_name, i))
        chunk = tail + read
        lines = chunk.splitlines()
        if chunk[-1] == '\n':  # 数据块最后一行是否完整切断
            df_chunk = pd.DataFrame(lines)[0].str.split(',', expand=True)
            tail = ''
        else:
            df_chunk = pd.DataFrame(lines[:-1])[0].str.split(',', expand=True)
            tail = lines[-1]
        chunk_len = len(df_chunk)
        df_chunk.index = range(j, j + chunk_len)
        # 当前样本序号范围包含抽样序号则抽样，否则跳过
        sample_id = list(set(range(j, j + chunk_len)) & set(sample_range))  # 序号交集
        if len(sample_id) > 0:
            df_sample = df_sample.append(df_chunk.loc[sample_id])
        j += chunk_len
    f.close()
    # 抽样结果输出保存
    if len(df_sample) > 0:
        try:
            df_sample.to_csv(file_path + 'sample_' + file_name + '.txt', index=False, header=False)
            print("======%s\t%s处理完毕，原文件共有%d行，成功抽样%d行======" % (
                dt.datetime.now().strftime('%Y-%m-%d %H:%M:%S'), file_name, count, len(df_sample)))
        except Exception:
            print(traceback.print_exc())
    else:
        print("======%s\t%s处理完毕，原文件共有%d行，抽取样本0行======" % (
        dt.datetime.now().strftime('%Y-%m-%d %H:%M:%S'), file_name, count))


def merchant_split(file, encoding=None, m=20, city_cd_loc=4, in_rule='^[1-9]|0156|000[01]', out_rule='^0(?!00[01]|156)'):
    """
    商户按地区拆分境内外
    :param file: str, 待抽样的文件名，含路径及后缀
    :param encoding: str, 编码方式
    :param m: int, 每次读入处理的数据量，单位为兆
    :param city_cd_loc: int, 城市代码字段所在的位置，从0开始，例如在第五列，则输入4
    :param in_rule: str, 城市代码为境内的正则表达式
    :param out_rule: str, 城市代码为境外的正则表达式
    :return: 拆分结果，本地文件
    """
    # 提取文件路径及文件名
    file_name = str(os.path.basename(file).split('.')[0])
    file_path = os.path.dirname(file) + '/'

    # 初始化输出对象

    if os.path.exists(file_path + file_name + '_domestic.txt'):
        os.remove(file_path + file_name + '_domestic.txt')
    if os.path.exists(file_path + file_name + '_international.txt'):
        os.remove(file_path + file_name + '_international.txt')
    if os.path.exists(file_path + file_name + '_remained.txt'):
        os.remove(file_path + file_name + '_remained.txt')
    print("\n%s: 约%.1fM，开始处理..." % (file_name, os.path.getsize(file) / 1024 / 1024))
    # 遍历文件，每次读取一部分数据，基于city_code进行地区划分
    i = 0  # 分块计数
    count = 0  # 总行数
    count_in = 0  # 境内数据行数
    count_out = 0  # 境外数据行数
    count_remain = 0  # 无法判断境内外数据行数
    tail = ''  # 分块残留数据尾部
    f = open(file, 'r', encoding=encoding)
    while True:
        read = f.read(1024 * 1024 * m)
        if not read:
            break
        i += 1
        print("%s\t处理%s第%d部分" % (dt.datetime.now().strftime('%Y-%m-%d %H:%M:%S'), file_name, i))
        chunk = tail + read
        lines = chunk.splitlines()
        if chunk[-1] == '\n':
            df_chunk = pd.DataFrame(lines)[0].str.split(',', expand=True)
            tail = ''
        else:
            df_chunk = pd.DataFrame(lines[:-2])[0].str.split(',', expand=True)
            tail = lines[-1]
        count += len(df_chunk)
        # 划分境内外地区
        df_chunk_in = df_chunk[df_chunk[city_cd_loc].str.contains(in_rule, flags=re.IGNORECASE)]
        df_chunk_out = df_chunk[df_chunk[city_cd_loc].str.contains(out_rule, flags=re.IGNORECASE)]
        if len(df_chunk_in) > 0:
            count_in += len(df_chunk_in)
            df_chunk.drop(df_chunk_in.index, inplace=True)
            df_chunk_in.to_csv(file_path + file_name + '_domestic.txt', index=False, header=False, mode='a')  # 追加写入

        if len(df_chunk_out) > 0:
            count_out += len(df_chunk_out)
            df_chunk.drop(df_chunk_out.index, inplace=True)
            df_chunk_out.to_csv(file_path + file_name + '_international.txt', index=False, header=False, mode='a')  # 追加

        if len(df_chunk) > 0:
            count_remain += len(df_chunk)
            df_chunk.to_csv(file_path + file_name + '_remained.txt', index=False, header=False, mode='a')  # 追加
    f.close()
    print("======%s\t%s处理完毕，共有%d行，其中domestic:international:remained =  %.1f%% : %.1f%% : %.1f%% = %d : %d : %d"
          % (dt.datetime.now().strftime('%Y-%m-%d %H:%M:%S'), file_name, count, (100 * count_in / (count + 0.001)),
             (100 * count_out / (count + 0.001)), (100 * count_remain / (count + 0.001)), count_in, count_out, count_remain))


def industry_merchant_clean(file, columns, df_rule, encoding=None, m=20, show=True, keyword=False):
    """
    商户清洗函数，根据规则对商户文本文件进行清洗
    :param file: str, 待抽样的文件名，含路径及后缀
    :param columns: list, 文件列名
    :param df_rule: DataFrame, 清洗规则文件
    :param encoding: str, 编码方式
    :param m: int, 每次读入处理的数据量，单位为兆
    :param show: bool, 是否打印中间过程
    :param keyword: bool, clean文件是否增加一列name_white
    :return: 清洗结果，本地文件
    """
    # 提取文件路径及文件名
    file_name = str(os.path.basename(file).split('.')[0])
    file_path = os.path.dirname(file) + '/'

    # 匹配待清洗商户相应的规则
    df_rule_industry = df_rule[df_rule['file_name'] == file_name]
    if len(df_rule_industry) == 0:
        print('\n%s: 找不到清洗规则，清洗跳过！' % file_name)
        return

    # 输出路径初始化
    path_clean = file_path + 'clean/'
    path_black = file_path + 'black/'
    if not os.path.exists(path_clean):
        os.mkdir(path_clean)
    if not os.path.exists(path_black):
        os.mkdir(path_black)
    if show:
        print("\n%s: 约%.1fM，开始清洗..." % (file_name, os.path.getsize(file) / 1024 / 1024))
    # 情况二：样本总数超过抽样个数时，基于随机序号进行抽样
    # 遍历文件，每次读取一部分，基于随机样本序号进行抽样
    f = open(file, 'r', encoding=encoding)
    i = 0  # 块数计数
    tail = ''  # 分块残留数据尾部
    while True:
        read = f.read(1024 * 1024 * m)
        if not read:
            break
        i += 1
        print("%s\t处理%s第%d部分" % (dt.datetime.now().strftime('%Y-%m-%d %H:%M:%S'), file_name, i))
        chunk = tail + read
        lines = chunk.splitlines()
        if chunk[-1] == '\n':  # 数据块最后一行是否完整切断
            df_chunk = pd.DataFrame(lines)[0].str.split(',', expand=True)
            tail = ''
        else:
            df_chunk = pd.DataFrame(lines[:-1])[0].str.split(',', expand=True)
            tail = lines[-1]
        df_chunk.columns = columns
        # 执行清洗循环，第一层遍历每个地区（境内+境外），提取相应的清洗规则（可能存在多个规则）
        for district in df_rule_industry['district'].unique():
            # 创建数据集存放当前地区的清洗结果
            df_clean = pd.DataFrame()
            df_black = pd.DataFrame()

            df_rule_district = df_rule_industry[df_rule_industry['district'] == district]
            # 第二层循环在地区内执行相应的清洗规则
            for j in range(len(df_rule_district)):
                count_raw = len(df_chunk)
                # 规则一：城市白名单
                city_code_white = df_rule_district['citycode_white'].iloc[j]
                df_city_code = df_chunk[df_chunk['city_cd'].str.contains(city_code_white, flags=re.IGNORECASE)]
                df_chunk.drop(df_city_code.index, inplace=True)  # 城市白名单匹配失败的，等待执行下一轮清洗规则或输出为未匹配

                # 规则二：商户名称白名单
                name_white = df_rule_district['name_white'].iloc[j]
                df_name_white = df_city_code[
                    df_city_code['mchnt_name'].str.contains(name_white, flags=re.IGNORECASE)]
                if keyword:
                    df_name_white['keywords'] = name_white
                df_city_code.drop(df_name_white.index, inplace=True)
                if len(df_city_code) > 0:
                    df_city_code['drop_reason'] = '不在name_white内'
                    df_black = df_black.append(df_city_code)

                # 规则三：商户名称黑名单
                name_black = df_rule_district['name_black'].iloc[j]
                df_name_black = df_name_white[
                    ~df_name_white['mchnt_name'].str.contains(name_black, flags=re.IGNORECASE)]
                df_name_white.drop(df_name_black.index, inplace=True)
                if len(df_name_white) > 0:
                    df_name_white['drop_reason'] = 'name_black'
                    df_black = df_black.append(df_name_white)

                # 规则四：商户类型
                mcc_white = df_rule_district['mcc_white'].iloc[j]
                df_mcc_white = df_name_black[df_name_black['mcc'].str.contains(mcc_white, flags=re.IGNORECASE)]
                df_name_black.drop(df_mcc_white.index, inplace=True)
                if len(df_name_black) > 0:
                    df_name_black['drop_reason'] = 'MCC 不在范围内'
                    df_black = df_black.append(df_name_black)

                df_clean = df_clean.append(df_mcc_white)  # 保留当前规则清洗后的商户

            count_clean = len(df_clean)
            count_black = len(df_black)
            count_unmatch = len(df_chunk)
            if show:
                print("\t清洗%s，共%d行，其中clean:black:unmatch = %.1f%% : %.1f%% : %.1f%% = %d : %d : %d"
                      % (district, count_raw, (100 * count_clean / (count_raw + 0.001)),
                         (100 * count_black / (count_raw + 0.001)),
                         (100 * count_unmatch / (count_raw + 0.001)), count_clean, count_black, count_unmatch))
            if count_clean > 0:
                df_clean.to_csv(path_clean + file_name + '_' + str(district) + '_clean.txt', mode='a', index=None,
                                header=False)
            if count_black > 0:
                df_black.to_csv(path_black + file_name + '_' + str(district) + '_black.txt', mode='a', index=None,
                                header=False)
            if count_unmatch > 0:
                df_chunk.to_csv(path_black + file_name + '_' + str(district) + '_unmatch.txt', mode='a', index=None,
                                header=False)
    f.close()


def str_replace(df, columns, str_raw="(", str_rep="\\\\("):
    """
    替换指定列中的指定字符
    :param df: DataFrame
    :param columns: list, 可能含有需要替换字符的列名
    :param str_raw: str, 需要被替换的原始字符
    :param str_rep: 需要替换成的目标字符
    :return: DataFrame
    """
    for col in columns:
        df[col] = df[col].apply(lambda x: x.replace(str_raw, str_rep))
    return df
